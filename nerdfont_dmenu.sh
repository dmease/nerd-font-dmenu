#!/bin/sh

dmenu < ~/.cache/nerdfont.map | cut -d' ' -f1 | xclip -selection clipboard
