#!/bin/sh

cat i_*.sh > all.raw
grep -e "^i" all.raw | sed -r "s/.*'(.+)'.*i_.*_(.*)=.*/\1 \2/" | grep -v i= > nerdfont.map
