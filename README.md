# Nerd Font dmenu

Look up a Nerd Font glyph using dmenu.


A dmenu script is included, but you can also just grab the
`nerdfont.map` file and do your own thing with it. Each line in the map
file contains a Nerd Font glyph, a space, and the name of the glyph.

## Installation

1. Clone this repo.
#. Go into the main directory:
    ```
    cd nerdfontdmenu
    ```
#. Make sure the script is executable:
    ```
    chmod a+x nerdfont_dmenu.sh
    ```
#. Copy `nerdfont.map`  into `~/.cache/` (or wherever you like, bu update the script with the new location).
    ```
    mkdir -p ~/.cache/
    cp nerdfont.map ~/.cache/
    ```
# Copy the script to `~/bin/` (or wherever you like).
    ```
    mkdir -p ~/bin/
    cp nerdfont_dmenu.sh ~/bin/
    ```

## Usage

Run the script:

```
~/bin/nerdfont_dmenu.sh
```

Start typing the name of a glyph, then press `Enter`. The selected glyph
will be copied to your clipboard.

# Technical

Make sure you have a patched font. See
https://github.com/ryanoasis/nerd-fonts.git.

`nerdfont.map` is generated from the following file in the Nerd Fonts
repository under the directory `nerd-font/bin/scripts/lib/`:

- i_dev.sh
- i_fae.sh
- i_fa.sh
- i_iec.sh
- i_linux.sh
- i_material.sh
- i_oct.sh
- i_ple.sh
- i_pom.sh
- i_seti.sh
- i_weather.sh

To regenerate `nerdfont.map` file, download the above files to the
nerdfontdmenu folder and run the `gen_map.sh` script.

